#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2022 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect.csr_eventmanager import EventManager, EventSourceLevel
from litex.soc.interconnect import wishbone, stream
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

kB = 1024
mB = 1024*kB

# I2C interface ------------------------------------------------------------------------------------

class OpenCoresI2CMaster(Module, AutoCSR):
    def __init__(self, platform, pads, clk_freq):
        self.bus    = bus    = wishbone.Interface(data_width=8, adr_width=5)
        self.wb_irq = Signal()

        # Set up IRQ handling
        self.submodules.ev = EventManager()
        self.ev.master_interrupt = EventSourceLevel(name="IRQ", description="I2C master core interrupt")
        self.ev.finalize()

        # I2C bus signals
        self.i2c_sda_out = Signal()
        self.i2c_sda_direction = Signal()
        self.i2c_sda_in = Signal()
        self.i2c_scl_out = Signal()
        self.i2c_scl_direction = Signal()
        self.i2c_scl_in = Signal()

        self.specials += Instance("i2c_master_top",
            # Configuration data
            i_sys_clk_freq = clk_freq,

            # Wishbone signals
            i_wb_cyc_i  = bus.cyc,
            i_wb_stb_i  = bus.stb,
            i_wb_we_i   = bus.we,
            i_wb_adr_i  = bus.adr,
            i_wb_dat_i  = bus.dat_w,
            o_wb_dat_o  = bus.dat_r,
            o_wb_ack_o  = bus.ack,
            o_wb_inta_o = self.wb_irq,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_arst_i   = ResetSignal('sys'),
            i_wb_rst_i = ResetSignal('sys'),
            i_wb_clk_i = ClockSignal('sys'),

            # I2C interface
            o_sda_pad_o   = self.i2c_sda_out,
            o_sda_padoe_o = self.i2c_sda_direction,
            i_sda_pad_i   = self.i2c_sda_in,
            o_scl_pad_o   = self.i2c_scl_out,
            o_scl_padoe_o = self.i2c_scl_direction,
            i_scl_pad_i   = self.i2c_scl_in
        )
        # Add Verilog source files
        self.add_sources(platform)

        # I/O drivers
        self.specials += SDRTristate(
            io = pads.sda,
            o  = self.i2c_sda_out,
            oe = self.i2c_sda_direction,
            i  = self.i2c_sda_in,
        )
        self.specials += SDRTristate(
            io = pads.scl,
            o  = self.i2c_scl_out,
            oe = self.i2c_scl_direction,
            i  = self.i2c_scl_in,
        )

        # Generate IRQ
        # Active high logic
        self.comb += self.ev.master_interrupt.trigger.eq(self.wb_irq)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "opencoresi2c").data_location
        platform.add_source(os.path.join(vdir, "third_party/i2c_master/i2c_master_top.v"))
        platform.add_source(os.path.join(vdir, "third_party/i2c_master/i2c_master_bit_ctrl.v"))
        platform.add_source(os.path.join(vdir, "third_party/i2c_master/i2c_master_byte_ctrl.v"))
